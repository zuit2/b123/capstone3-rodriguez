import React,{useState,useEffect,useContext} from 'react';
import SoldProduct from '../components/SoldProduct'
import UserContext from '../userContext'
import {Card,Table,Button,Row,Col} from 'react-bootstrap'
import Swal from 'sweetalert2'
import {Redirect,useHistory,useParams} from 'react-router-dom'


export default function ViewOrder(){

	const orderId = useParams().orderId;
	console.log(orderId);

	const [products,setProducts] = useState([]);
	const [order,setOrder] = useState([]);
	const [update,setUpdate] = useState(0);

	

	useEffect(()=>{

		fetch(`https://guarded-taiga-49324.herokuapp.com/users/order/${orderId}`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(foundOrder=> {
			console.log('-foundOrder')
			setOrder(foundOrder);
			// console.log(order);

			
			setProducts(foundOrder.products.map(product => {
				console.log(product);
				return (
					<Row className="justify-content-center d-flex align-items-center mb-5">
							<SoldProduct key={product._id} productProp={product}/>						
					</Row>
				)
			
			}))
		})

	},[])

	

	return(
		<>
			<h3 className="my-5 text-center">Order #{orderId}</h3>
			<Row className="justify-content-center">
				<Col md={8}>
					<Card className="mb-5" border="dark" bg="light">
					<Card.Body>
						<Card.Subtitle>Date:</Card.Subtitle>
						<Card.Text>{order.dateCreated}:</Card.Text>
						<Card.Subtitle>Total Amount (PHP):</Card.Subtitle>
						<Card.Text>{order.totalAmount}</Card.Text>
						<Card.Subtitle>User ID:</Card.Subtitle>
						<Card.Text>{order.userId}</Card.Text>
					</Card.Body>
					</Card>
				</Col>
			</Row>	
			{products}
			
		</>
		)

}