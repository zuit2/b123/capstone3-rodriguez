import React,{useState,useEffect} from 'react';
import AppNavBar from './components/AppNavBar'
import { BrowserRouter as Router } from 'react-router-dom';
import {Route,Switch} from 'react-router-dom';
import {UserProvider} from './userContext'
import {Container} from 'react-bootstrap'
import './App.css'

import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Products from './pages/Products'
import AddProduct from './pages/AddProduct'
import ViewProduct from './pages/ViewProduct'
import UpdateProduct from './pages/UpdateProduct'
import Cart from './pages/Cart'
import Orders from './pages/Orders'
import ViewOrder from './pages/ViewOrder'
import Logout from './pages/Logout'
import NotFound from './pages/NotFound'

export default function App() {


  const [user,setUser] = useState({
    id: null,
    isAdmin: null
  })

  useEffect(()=>{
    fetch('https://guarded-taiga-49324.herokuapp.com/users/getUserDetails',{

      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      // console.log(data)
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  },[])

  console.log(user)

  const unsetUser = () => {

    localStorage.clear()
  }

  return (
    <>
      <UserProvider value={{user,setUser,unsetUser}}>
        <Router>
          <AppNavBar/>
            <Container className="mb-5 pb-5" >
              <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/products" component={Products}/>
                <Route exact path="/addproduct" component={AddProduct}/>
                <Route exact path="/product/:productId" component={ViewProduct}/>
                <Route exact path="/updateproduct/:productId" component={UpdateProduct}/>
                <Route exact path="/cart" component={Cart}/>
                <Route exact path="/orders" component={Orders}/>
                <Route exact path="/order/:orderId" component={ViewOrder}/>
                <Route exact path="/logout" component={Logout}/>
                <Route component={NotFound}/>
              </Switch>
            </Container>
        </Router>
      </UserProvider> 
    </>
  );

}

