# Project Name: Checkpoint E-commerce Web-app

## Features:

User registration\
User authentication\
Retrieve all active products\
Retrieve single product\
Add product (Admin only)\
Update product Information (Admin only)\
Disable product (Admin only)\
Enable product (Admin only)\
Create order - Shopping Cart (Registered User only)\
Get all orders (Admin only)\
Get user orders (Registered User only) \
View order (Registered User)

### Admin Credentials:

email: "adminAPI@gmail.com" \
password: "adminAPI123"
